import React, { Component } from 'react';
import './Bootstrap.css';
import {Navbar, Nav} from "react-bootstrap";
import './Header.css';

class Header extends Component{
    render(){
      return(
          // <div>
          //   <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          //       <a class="navbar-brand" href="#">Navbar</a>
          //       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          //           <span class="navbar-toggler-icon"></span>
          //       </button>
          //       <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          //           <div class="navbar-nav">
          //           <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
          //           <a class="nav-item nav-link" href="#">Features</a>
          //           <a class="nav-item nav-link" href="#">Pricing</a>
          //           <a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
          //           </div>
          //       </div>
          //   </nav>
          // </div>

          <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Elearning</Navbar.Brand>
            <Navbar.Collapse className="justify-content-center">
            <Nav activeKey="/home">
                  <Nav.Item>
                    <Nav.Link href="/home">Home</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="link-1">About</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="link-2">How To</Nav.Link>
                  </Nav.Item>
                
              </Nav>
            </Navbar.Collapse>
              
          </Navbar>

          

      );
    }
  }

  export default Header;