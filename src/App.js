import React from 'react';
import Header from "./Header"
import Footer from "./Footer"
import {Button,Jumbotron,Container} from "react-bootstrap";
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div>
        <Header/>
          <h1>Lets Join</h1>
          <h1 className="txt2">With Our Community</h1>
          <p>study now be proud later</p>
          <Button variant="outline-warning" >Login</Button>
          <Button className="btnsign" variant="outline-warning">Sign Up</Button>

          <div className="backgr"><img src={require('./img/img-5.gif')} alt="Credit to Joshua Earle on Unsplash"/></div>

        <div className="col-12">
            <center>
              <h2>About</h2>
            </center>
            <p className="txtabout">Elen adalah situs E-Learning yang meneyediakan fasilitas belajar bagi mahasiswa di Telkom University
              <br></br>yang dibuat dengan tujuan agar mahasiswa bisa lebih mudah untuk mencari materi perkuliahan yang tidak hanya tersedia dalam bentuk teks.
              <br></br>Tetapi juga dalam bentuk video pembelajaran agar bisa membantu pemahaman mahasiswa.
            </p>
            
            <center>
              <h2 className="how">How To</h2>
            </center>
            <p className="txthow">Langkah-Langkah untuk memulai belajar</p>
            <div><img className="imac" src={require('./img/Landing page.png')} alt="Credit to Joshua Earle on Unsplash"/></div>
            
        </div>
        
        <Footer/>
      </div>
    );
  }
}

export default App;
